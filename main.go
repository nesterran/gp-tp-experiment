package main

import (
	"bytes"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/tealeg/xlsx"

	"encoding/json"

	"net/http"

	"fmt"
	"io/ioutil"
	"os"
)

type Comment struct {
	CommentID  string
	Text       string
	ObfText    string
	DeobfText  string
	GPScore    string
	GPObfScore string
	GPTPScore  string
	GPTime     time.Duration
	GPTPTime   time.Duration
}

type ScoreResult struct {
	ID      string
	comment Comment
	errors  []error
}

func (c *ScoreResult) String() string {
	return fmt.Sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%v\t%s\t%s",
		c.ID,
		c.comment.GPScore,
		c.comment.GPObfScore,
		c.comment.GPTPScore,
		c.comment.Text,
		c.comment.ObfText,
		c.comment.DeobfText,
		c.errors,
		c.comment.GPTime,
		c.comment.GPTPTime)
}

func (c *ScoreResult) HeadersString() string {
	headers := []string{
		"CommentID",
		"GPScore",
		"GPObfScore",
		"GPTPScore",
		"Text",
		"ObfText",
		"DeobfText",
		"Error",
		"GPTime",
		"GPTPTime",
	}
	return strings.Join(headers, "\t")
}

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Incorrect amount of parameters. Expected 2. First: experiments file path. Second: output result file path. %v", os.Args)
	}
	fileName := os.Args[1]
	outputFileName := os.Args[2]
	chScore := CalculateScores(fileName)
	done := SaveComment(chScore, outputFileName)
	<-done
}

func SaveComment(chScore chan ScoreResult, fileName string) chan bool {
	done := make(chan bool)
	go func() {
		defer func() { done <- true }()
		file, err := os.Create(fileName)
		if err != nil {
			log.Printf("Error creating file: %v", err)
			return
		}
		defer file.Close()

		emtpyScore := &ScoreResult{}
		file.WriteString(emtpyScore.HeadersString() + "\n")

		for score := range chScore {
			log.Printf("Saving(%s) Err:%v", score.ID, score.errors)
			_, err := file.WriteString(score.String() + "\n")
			if err != nil {
				log.Printf("Error writing to output file: %v", err)
			}
		}
	}()
	return done
}

func CalculateScores(excelFileName string) chan ScoreResult {
	chScore := make(chan ScoreResult)
	go func() {
		defer close(chScore)
		xlFile, err := xlsx.OpenFile(excelFileName)
		if err != nil {
			log.Fatalf("Could not open file %s. %v", excelFileName, err)
		}
		skip := false
		skipTillID := ""
		if os.Getenv("TP_SKIP_TILL") != "" {
			skip = true
			skipTillID = os.Getenv("TP_SKIP_TILL")
			log.Println("Skiping till ", skipTillID)
		}
		if err != nil {
			log.Printf("Error opening file: %v", err)
			return
		}

		type comm struct {
			Text  string
			Score string
		}
		toxicComments := make(map[string]comm)

		for _, sheet := range xlFile.Sheets {
			if sheet.Name == "Toxic comments" {

				for idx, row := range sheet.Rows {
					if idx == 0 {
						continue
					}
					if idx == 25 {
						break
					}
					commentID, err := row.Cells[0].String()
					if err != nil {
						log.Printf("Could not get toxic comment on row %v. Error on field commentID: %v", idx, err)
						continue
					}
					text, err := row.Cells[1].String()
					if err != nil {
						log.Printf("Could not get toxic comment on row %v. Error on field text: %v", idx, err)
						continue
					}
					score, _, err := getScore(text)
					if err != nil {
						log.Printf("Could not get toxic comment on row %v. Error on field score: %v", idx, err)
						continue
					}

					toxicComments[commentID] = comm{Text: text, Score: score}
				}

				continue
			}

			if !strings.HasPrefix(sheet.Name, "usel") && !strings.HasPrefix(sheet.Name, "brex") && !strings.HasPrefix(sheet.Name, "clic") && !strings.HasPrefix(sheet.Name, "Negated toxic comments") {
				continue
			}

			row := sheet.Rows[0]
			var textIndex int
			for idx, cell := range row.Cells {
				if val, _ := cell.String(); err == nil && val == "MERGED VERSION" {
					textIndex = idx
					break
				}
			}

			if textIndex == 0 {
				continue
			}

			for i := 1; i < len(sheet.Rows); i++ {
				row = sheet.Rows[i]
				cell := row.Cells[textIndex]

				obfText, err := cell.String()
				if err != nil {
					log.Printf("Error: %v", err)
					continue
				}

				commentID, err := row.Cells[0].String()
				if err != nil {
					log.Printf("Error: %v", err)
					continue
				}

				variationID, err := row.Cells[1].String()
				if err != nil {
					log.Printf("Error: %v", err)
					continue
				}

				ID := commentID + variationID
				if skip {
					if ID == skipTillID {
						log.Printf("Skipping %s", ID)
						skip = false
					}
					continue
				}

				errors := make([]error, 0, 3)
				GPScore, GPTime, err := getScore(obfText)
				if err != nil {
					errors = append(errors, err)
				}

				deobfText, TPTime, err := deobfuscate(obfText)
				if err != nil {
					errors = append(errors, err)
				}
				GPTPScore, GPTPTime, err := getScore(deobfText)
				if err != nil {
					errors = append(errors, err)
				}

				originalComment := toxicComments[strings.TrimSuffix(commentID, "-")]
				comm := Comment{
					CommentID:  ID,
					Text:       originalComment.Text,
					ObfText:    obfText,
					DeobfText:  deobfText,
					GPScore:    originalComment.Score,
					GPObfScore: GPScore,
					GPTPScore:  GPTPScore,
					GPTime:     GPTime,
					GPTPTime:   GPTPTime + TPTime,
				}

				chScore <- ScoreResult{ID, comm, errors}
			}
		}
		close(chScore)
	}()
	return chScore
}

type PerspectiveResponse struct {
	AttributeScores struct {
		Toxicity struct {
			SummaryScore struct {
				Value float64 `json:"value"`
			} `json:"summaryScore"`
		} `json:"TOXICITY"`
	} `json:"attributeScores"`
}

func getScore(text string) (string, time.Duration, error) {
	type Request struct {
		Comment string `json:"comment"`
	}

	request := Request{Comment: text}

	body, err := json.Marshal(&request)
	if err != nil {
		log.Printf("Error marshalling JSON:%v", err)
		return "", 0, err
	}

	start := time.Now()
	resp, err := http.Post("https://www.perspectiveapi.com/check", "application/json", bytes.NewReader(body))
	elapsed := time.Since(start)
	if err != nil {
		log.Printf("Error creating request:%v", err)
		return "", elapsed, err
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		log.Printf("Error with Text: %s. Response Code: %v, %s", text, resp.StatusCode, resp.Status)
		return "", elapsed, fmt.Errorf("Perspective error: %s with text %s", resp.Status, text)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading all bytes:%v", err)
		return "", elapsed, err
	}

	var perspectiveResp PerspectiveResponse
	err = json.Unmarshal(body, &perspectiveResp)
	if err != nil {
		log.Printf("Error unmarshalling json: %s, %v\n", string(body), err)
		return "", elapsed, err
	}

	score := perspectiveResp.AttributeScores.Toxicity.SummaryScore.Value
	return strconv.FormatFloat(score, 'f', 2, 64), elapsed, nil
}

func deobfuscate(text string) (string, time.Duration, error) {
	type TPRequest struct {
		Text string `json:"text"`
	}

	type TPResponse struct {
		Text string `json:"text"`
	}

	request := TPRequest{Text: text}

	body, err := json.Marshal(&request)
	if err != nil {
		log.Printf("Error marshalling JSON:%v", err)
		return "", 0, err
	}

	req, err := http.NewRequest(http.MethodPost, "https://api.textpatrol.tk/deobfuscate", bytes.NewReader(body))

	tpkey := os.Getenv("TP_KEY")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Api-Key", tpkey)

	client := http.Client{}

	start := time.Now()
	resp, err := client.Do(req)
	elapsed := time.Since(start)

	if err != nil {
		log.Printf("Error doing request:%v", err)
		return "", elapsed, err
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		log.Printf("Error TP Response Code: %v, %s", resp.StatusCode, resp.Status)
		return "", elapsed, fmt.Errorf("TP Error: %s with text %s", resp.Status, text)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading all bytes:%v", err)
		return "", elapsed, err
	}

	var tpResponse TPResponse
	err = json.Unmarshal(body, &tpResponse)
	if err != nil {
		log.Printf("Error unmarshalling json: %s, %v\n", string(body), err)
		return "", elapsed, err
	}

	return tpResponse.Text, elapsed, nil
}
